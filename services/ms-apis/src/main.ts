import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Logger } from '@nestjs/common';
import { Transport, MicroserviceOptions } from '@nestjs/microservices';

const logger = new Logger('Main');
const microserviceOptions: MicroserviceOptions = {
  transport: Transport.TCP,
  options: {
    host: 'ms-apis',
    port: 4200,
  },
};

async function bootstrap() {
  const app = await NestFactory.createMicroservice<MicroserviceOptions>(
    AppModule,
    microserviceOptions,
  );
  app.listen(() => logger.verbose('Microservice is running on 4200'));
}
bootstrap();
